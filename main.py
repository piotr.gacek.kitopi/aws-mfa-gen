#!/usr/bin/python

import logging
import random
import os
import sys
import boto3

try:
  import ConfigParser
except:
  import configparser as ConfigParser
from os.path import expanduser

##########################################################################

profile = os.environ['AWS_PROFILE'] if 'AWS_PROFILE' in os.environ else ''

profile_section = 'profile ' + profile if profile != '' else 'default'

region = os.environ['AWS_REGION'] if 'AWS_REGION' in os.environ else 'eu-west-1'

aws_credentials_file = os.environ['AWS_SHARED_CREDENTIALS_FILE'] if 'AWS_SHARED_CREDENTIALS_FILE' in os.environ else '~/.aws/credentials'
aws_config_file = os.path.dirname(aws_credentials_file) + '/config'

# Uncomment to enable low level debugging
#logging.basicConfig(level=logging.DEBUG)

##########################################################################

config = ConfigParser.RawConfigParser()
config.read(expanduser(aws_config_file))

if not config.has_section(profile_section):
  print('No profile section defined in configuration file')
  sys.exit(1)

if config.has_option(profile_section, 'source_profile'):
  source_profile = config.get(profile_section, 'source_profile')
else:
  print('No source_profile specified for the selected profile.')
  sys.exit(1)

if config.has_option(profile_section, 'mfa_serial'):
  mfa_serial = config.get(profile_section, 'mfa_serial')
else:
  print('No mfa_serial specified for the selected profile.')
  sys.exit(1)

if config.has_option(profile_section, 'role_arn'):
  role_arn = config.get(profile_section, 'role_arn')
else:
  print('No role_arn specified for the selected profile.')
  sys.exit(1)

if config.has_option(profile_section, 'duration_seconds'):
  duration_seconds = config.get(profile_section, 'duration_seconds')
else:
  duration_seconds = 60 * 60

if config.has_option(profile_section, 'region'):
  region = config.get(profile_section, 'region')

print(f'Signing in using MFA device: {mfa_serial}')

client = boto3.client('sts')

response = client.assume_role(
  RoleArn=role_arn,
  RoleSessionName=f'{profile}-{random.randint(1, 9999999)}',
  DurationSeconds=duration_seconds
)

credentials = response['Credentials']
print(credentials)

# Read in the existing config file
credentials_config_path = expanduser(aws_credentials_file)

credentials_config = ConfigParser.RawConfigParser()
credentials_config.read(credentials_config_path)

if not credentials_config.has_section(profile):
    credentials_config.add_section(profile)

credentials_config.set(profile, 'aws_access_key_id', credentials['AccessKeyId'])
credentials_config.set(profile, 'aws_secret_access_key', credentials['SecretAccessKey'])
credentials_config.set(profile, 'aws_session_token', credentials['SessionToken'])
credentials_config.set(profile, 'expiration', credentials['Expiration'])

# Write the updated config file
with open(credentials_config_path, 'w+') as config_file:
    credentials_config.write(config_file)


# Give the user some basic info as to what has just happened
print('\n\n--------------------------------------------------------------------------------------------------------')
print(f'Your are now signed in. The token will expire at {credentials["Expiration"]}')
print('After this time, you may safely rerun this script to refresh your access key pair.')
print('--------------------------------------------------------------------------------------------------------\n\n')